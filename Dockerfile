FROM node:16-alpine

# Create directory
RUN mkdir -p /app

# Copy app content
COPY app/* app/

# Set default directory
WORKDIR /app

EXPOSE 3000

# Install dependencies
RUN npm install


CMD ["node", "server.js"]
